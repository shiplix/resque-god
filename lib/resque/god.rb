require 'resque/god/version'
require 'resque/god/railtie'
require 'active_support/dependencies/autoload'

module Resque
  module God
    extend ActiveSupport::Autoload

    autoload :Configuration

    def self.config
      @config ||= Configuration.new(Rails.root.join('config', 'resque-god.yml'))
    end
  end
end
