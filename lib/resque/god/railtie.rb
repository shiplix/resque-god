require 'rails/railtie'

module Resque
  module God
    class Railtie < Rails::Railtie
      rake_tasks do
        load 'resque/god/tasks.rake'
      end
    end
  end
end
