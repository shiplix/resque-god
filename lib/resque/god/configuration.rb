require 'yaml'
require 'ostruct'
require 'erb'

require 'active_support/core_ext/hash/keys'
require 'active_support/core_ext/hash/deep_merge'

module Resque
  module God
    class Configuration
      class Worker < OpenStruct
        def initialize(queue, config)
          data = {queue: queue}

          if config.is_a?(Hash)
            data.merge!(config.symbolize_keys)
          else
            data[:count] = config
          end

          super(data)
        end

        def count
          [super || 1, 1].max
        end

        def env
          env = super || {}

          env[:QUEUE] ||= queue

          Hash[env.map { |k, v| [k, v.to_s] }]
        end
      end

      def initialize(*paths)
        @configuration = {}
        paths.each { |f| load f }
      end

      def workers
        @workers ||= (self[:workers] || {}).map { |k, v| Worker.new(k, v) }
      end

      def log_file
        self['resque.log_file'] || ::Rails.root.join('log/resque.log').to_s
      end

      def config_file
        self['resque.config_file'] || ::Rails.root.join('config/resque.god').to_s
      end

      def pid_file
        "#{pids}/resque-god.pid"
      end

      def pids
        self['resque.pids'] || ::Rails.root.join('tmp/pids').to_s
      end

      def root
        self['resque.root'] || ::Rails.root.to_s
      end

      def terminate_timeout
        workers.map(&:stop_timeout).compact.max.to_i + 10
      end

      def env
        env = self['env'] || {}

        Hash[env.map { |k, v| [k, v.to_s] }]
      end

      def to_god
        template = ERB.new(File.read(File.join(File.dirname(__FILE__), 'god.erb')))
        template.result(binding)
      end

      private

      def load(path)
        if File.exists?(path)
          config = YAML.load(File.read(path))

          @configuration.merge!(config)
        end
      end

      # get value from configuration
      def [](path)
        parts = path.to_s.split('.')
        result = @configuration

        parts.each do |k|
          result = result[k]

          break if result.nil?
        end

        result
      end
    end
  end
end
