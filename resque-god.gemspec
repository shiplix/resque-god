# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'resque/god/version'

Gem::Specification.new do |spec|
  spec.name          = "resque-god"
  spec.version       = Resque::God::VERSION
  spec.authors       = ["bibendi"]
  spec.email         = ["bibendi@bk.ru"]
  spec.summary       = "Launch Resque workers from config via God"
  spec.description   = "Launch Resque workers from config via God. Worker`s settings are stored in the config file."
  spec.homepage      = "https://bitbucket.org/shiplix/resque-god"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'resque', '~> 1.25'
  spec.add_runtime_dependency 'god', '>= 0.13'
  spec.add_runtime_dependency 'railties', '>= 3.0'
  spec.add_runtime_dependency 'activesupport', '>= 3.0'

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency 'rspec', '~> 3.1'
end
